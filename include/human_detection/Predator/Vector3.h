#ifndef Vector3__H
#define Vector3__H

#include <cmath>

#define VPI 3.14159265359f


namespace tld {

float toRad(float angle);
float toDegree(float rad);

class Vector3_
{
    public:

        static const Vector3_ ZERO;

		//Rotation type
        static const int R_X;
        static const int R_Y;
        static const int R_Z;

         Vector3_(float x, float y, float z);
        //Vector3_(Vector3_& other);
         Vector3_();
         ~Vector3_();

         operator float*();

         float& operator[](int index);

         void operator=(Vector3_ other);
         float operator*(Vector3_ other);
         Vector3_ operator*(float k);
         Vector3_ operator/(float k);
         Vector3_ operator+(Vector3_ other);
         Vector3_ operator-(Vector3_ other);
         void operator*=(float k);
         void operator/=(float k) ;
         void operator+=(Vector3_ other);
         void operator-=(Vector3_ other);




         float norm();

         void normalize();

         float angleBetween(Vector3_ other) ;

         Vector3_ cross(Vector3_ other);

         void rotate(float angle, int type);
         void rotate(float angle, Vector3_ u);


         void transform(Vector3_ vi, Vector3_ vj, Vector3_ vk);

         Vector3_ proj(Vector3_ other);



    protected:
    private:

        float coord[3];


};
}

#endif // Vector3__H
