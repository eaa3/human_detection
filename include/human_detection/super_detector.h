#ifndef _SUPER_DETECTOR_
#define _SUPER_DETECTOR_

#include <vector>
#include "human_detection/cascade_detector.h"
#include "Predator/boundingbox.h"

using namespace cv;

class SuperDetector {

public:

	std::vector<CascadeDetector*> detectors;
	int detector_id;
	tld::BoundingBox detectedBB;

	void add_detector(CascadeDetector* detector);


	void detect(Mat image);




};




#endif