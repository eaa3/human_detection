#ifndef _HUMAN_DETECTION_SHAPE_CLASSIFIER_
#define _HUMAN_DETECTION_SHAPE_CLASSIFIER_

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;


class ShapeClassifier
{
public:



	double classify(Mat& patch);

	bool filter(Mat& patch);
};


#endif