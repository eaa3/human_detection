#ifndef _HUMAN_DETECTION_UTIL_
#define _HUMAN_DETECTION_UTIL_

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <cstdlib>
#include <ctime>

using namespace cv;

#define uint16 unsigned short int


//IO Funcs for Mats
void saveToFile(const std::vector<Mat>& modelset, std::string filename);
void saveToFile(const Mat& mat, std::string filename);
void loadFromFile(std::vector<Mat>& modelset, std::string filename);
Mat loadFromFile(std::string filename);



//Other useful funcs
uint16 minElement(const Mat& mat);
uint16 maxElement(const Mat& mat);

int randomInt(int from, int to);
Mat randomMatInt(int min, int max, int nrows, int ncols);

double ncc(Mat& v1, Mat& v2);
double sse(Mat& v1, Mat& v2);
void normalizePatch(Mat& v);

#endif