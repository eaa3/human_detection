#ifndef _HUMAN_DETECTION_NN_CLASSIFIER_
#define _HUMAN_DETECTION_NN_CLASSIFIER_

#include <string>
#include <vector>
#include <iostream>
#include <opencv2/core/core.hpp>
#include "human_detection/util.h"

using namespace cv;

class NNClassifier
{
public:
	std::vector<Mat> positives;
	std::vector<Mat> negatives;

	double tp_threshold;

	NNClassifier(double tp_threshold = 0.65);


	void load(std::string filename_positives, std::string filename_negatives);
	void save(std::string filename_positives, std::string filename_negatives);

	//Add the patch to the model accoding to it's label (1-positive,0-negative)
	void train(Mat patch, int label);


	double classify(Mat patch);

	bool filter(Mat patch);

};





#endif