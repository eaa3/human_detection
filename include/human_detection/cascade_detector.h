#ifndef _CASCADE_DETECTOR_
#define _CASCADE_DETECTOR_


#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
using namespace cv;



class CascadeDetector
{
private:

        CascadeClassifier detector_cascade;

public:

        vector<Rect> detectionResult;
        bool isOk;

        CascadeDetector( string filename );
        
        CascadeDetector( );

        void init( string filename );

        void detect( Mat& img );

};

#endif