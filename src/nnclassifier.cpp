#include "human_detection/nnclassifier.h"

NNClassifier::NNClassifier(double tp_threshold)
{
	this->tp_threshold = tp_threshold;
}

void NNClassifier::load(std::string filename_positives, std::string filename_negatives)
{
	loadFromFile(this->positives,filename_positives);
	loadFromFile(this->negatives,filename_negatives);

	/*
	for(int i = 0; i < positives.size(); i++)
	{
		normalizePatch(positives[i]);
	}

	for(int i = 0; i < negatives.size(); i++)
	{
		normalizePatch(negatives[i]);
	}
	*/
}
void NNClassifier::save(std::string filename_positives, std::string filename_negatives)
{
	saveToFile(this->positives,filename_positives);
	saveToFile(this->negatives,filename_negatives);
}


//Add the patch to the model accoding to it's label (1-positive,0-negative)
void NNClassifier::train(Mat patch, int label)
{
	if( label == 1 )
	{
		this->positives.push_back(patch);
	}
	else
	{
		this->negatives.push_back(patch);
	}
}


double NNClassifier::classify(Mat patch)
{
	double ncc_p = 0.0;
	double ncc_p2 = 0.0;
	double ncc_p3 = 0.0;
	double ncc_n = 0.0;
	double ncc_n2 = 0.0;
	double ncc_n3 = 0.0;

	double tmp = 0.0;

	Mat tmpPatch = patch.clone();
	normalizePatch(tmpPatch);

	for(int i = 0; i < positives.size(); i++)
	{
		Mat tmp_mat = positives[i].clone();
		normalizePatch(tmp_mat);
		tmp = ncc(tmp_mat,tmpPatch);

		if( tmp > ncc_p )
		{
			ncc_p = tmp;
		}
		else if( tmp > ncc_p2 )
		{
			ncc_p2 = tmp;
		}
		else if( tmp >= ncc_p3 )
		{
			ncc_p3 = tmp;
		}
	}

	tmp = 0.0;
	for(int i = 0; i < negatives.size(); i++)
	{
		Mat tmp_mat = negatives[i].clone();
		normalizePatch(tmp_mat);
		tmp = ncc(tmp_mat,tmpPatch);

		if( tmp > ncc_n )
		{
			ncc_n = tmp;
		}
		else if( tmp > ncc_n2 )
		{
			ncc_n2 = tmp;
		}
		else if( tmp >= ncc_n3 )
		{
			ncc_n3 = tmp;
		}
	}

	double dp = 1.0 - (ncc_p+ncc_p2+ncc_p3)/3;
	double dn = 1.0 - (ncc_n+ncc_n2+ncc_n3)/3;

	float dist = dn/(dp+dn);

	std::cout << "DP: " << dp <<" DN:" << dn << " DIST: " << dist << std::endl;

	return (ncc_p >= ncc_n) + (ncc_p2 >= ncc_n2) + (ncc_p3 >= ncc_n3);//dist;
}

bool NNClassifier::filter(Mat patch)
{
	double dist = classify(patch);

	/*
	if( dist >= 0.60 && dist < tp_threshold )
	{
		train(patch,1);
	}
	else if( dist >= 0.15 && dist < 0.25 )
	{
		train(patch,0);
	}
	*/
	std::cout << "3-NN Vote Count: " << dist << std::endl;
	return dist >= 2;//dist >= tp_threshold;
}


