#include "human_detection/util.h"

void saveToFile(const std::vector<Mat>& modelset, std::string filename)
{
    std::ofstream os(filename.c_str());
    
    os << modelset.size() << std::endl;

    for(int k = 0; k < modelset.size(); k++)
    {
      Mat mat = modelset[k];

      os << mat.rows << " " << mat.cols << " " << mat.type() << std::endl;

      for(int i = 0; i < mat.rows; i++)
      {
        for(int j = 0; j < mat.cols; j++)
        {
          os << mat.at<double>(i,j) << " ";
        }
        os << std::endl;
      }

    }
    
    os.close();
}

void saveToFile(const Mat& mat, std::string filename)
{
    std::ofstream os(filename.c_str());

    os << mat.rows << " " << mat.cols << " " << mat.type() << std::endl;

    for(int i = 0; i < mat.rows; i++)
    {
      for(int j = 0; j < mat.cols; j++)
      {
        os << mat.at<uint16>(i,j) << " ";
      }
      os << std::endl;
    }
    

    os.close();
}


void loadFromFile(std::vector<Mat>& modelset, std::string filename)
{
    modelset.clear();

    std::ifstream in(filename.c_str());

    int rows, cols, type, n;
    in >> n;
    
    for(int k = 0; k < n; k++)
    {

      in >> rows >> cols >> type;

      Mat loadedMat = Mat(rows,cols,type);

      for(int i = 0; i < rows; i++)
      {
        for(int j = 0; j < cols; j++)
        {
          in >> loadedMat.at<double>(i,j);
        }
      }

      modelset.push_back(loadedMat);
    }


   // imshow("loadedMat",loadedMat);

    in.close();
}

Mat loadFromFile(std::string filename)
{
    std::ifstream in(filename.c_str());

    int rows, cols, type;
    in >> rows >> cols >> type;

    Mat loadedMat(rows,cols,type);

    for(int i = 0; i < rows; i++)
    {
      for(int j = 0; j < cols; j++)
      {
        in >> loadedMat.at<uint16>(i,j);
      }
    }


   // imshow("loadedMat",loadedMat);

    in.close();

    //std::cout << loadedMat << std::endl;

    return loadedMat;
}

uint16 minElement(const Mat& mat)
{
    uint16 min = 0xFFFF;

    int size = mat.rows*mat.cols;
    
    for(int i = 0; i < size; i++)
    {
      if( mat.at<uint16>(i) > 0 && mat.at<uint16>(i) < min )
      {
        min = mat.at<uint16>(i);
      }
    }

    return min;

}

uint16 maxElement(const Mat& mat)
  {
    uint16 max = 0;

    int size = mat.rows*mat.cols;
    
    for(int i = 0; i < size; i++)
    {
      if( mat.at<uint16>(i) > max )
      {
        max = mat.at<uint16>(i);
      }
    }

    return max;

}

int randomInt(int from, int to)
{
    return rand()%(to - from + 1) + from;
}

Mat randomMatInt(int min, int max, int nrows, int ncols)
{
    Mat mat(nrows,ncols,DataType<uint16>::type);

    for(int i = 0; i < nrows; i++)
    {
      for(int j = 0; j < ncols; j++)
      {
        mat.at<uint16>(i,j) = (uint16)randomInt(min,max);
      }
    }

    return mat;
}


double ncc(Mat& v1, Mat& v2)
{
    double size1 = sqrt(v1.dot(v1));
    double size2 = sqrt(v2.dot(v2));

    double sp = (size1*size2);

    double dot = v1.dot(v2);

    return (dot/sp + 1)/2;
}

double sse(Mat& v1, Mat& v2)
{
  Mat diff = v1 - v2;

  double dist = diff.dot(diff);


  return dist;
}

void normalizePatch(Mat& v)
{
    double mean_v = mean(v)[0];

    v.convertTo(v,DataType<double>::type,1,-mean_v);
}