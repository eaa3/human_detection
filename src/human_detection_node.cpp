//ROS and OpenCV includes
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/contrib/contrib.hpp>
#include "std_msgs/String.h"
#include "std_msgs/Float64MultiArray.h"

//My includes
#include "human_detection/cascade_detector.h"
#include "human_detection/depth_estimator.h"
#include "human_detection/Predator/cpredator.h"
#include "human_detection/super_detector.h"
#include "human_detection/util.h"
#include "human_detection/nnclassifier.h"

//Traditional C/Cpp includes
#include <iostream>
#include <utility>
#include <cstdlib>
#include <ctime>

#define uint16 unsigned short int
#define PATCH_DIM 80

#define ORANGE Scalar(0,125,255)
#define BLUE Scalar(255, 100, 200)


bool use_nn_classifier = true;


static const std::string TRAINING_WINDOW = "Manual Training Window";
static const std::string OPENCV_WINDOW = "Image window";
static const std::string FACE_DETECTOR_CONFIG_FILE = "/opt/ros/groovy/share/OpenCV/haarcascades/haarcascade_frontalface_alt_tree.xml";
static const std::string UPPER_BODY_DETECTOR_CONFIG_FILE = "/opt/ros/groovy/share/OpenCV/haarcascades/haarcascade_upperbody.xml";
static const std::string PROFILE_FACE_DETECTOR_CONFIG_FILE = "/opt/ros/groovy/share/OpenCV/haarcascades/haarcascade_profileface.xml";
CascadeDetector fd(FACE_DETECTOR_CONFIG_FILE);
CascadeDetector upd(UPPER_BODY_DETECTOR_CONFIG_FILE);
CascadeDetector pfd(PROFILE_FACE_DETECTOR_CONFIG_FILE);

SuperDetector sd;
NNClassifier nnclassifier;

pair<float,float> realDimensions[] = {pair<float,float>(0.15f,0.20f),pair<float,float>(0.15f,0.20f),pair<float,float>(0.40f,0.60f)};

char filepath[256];
int positiveCounter;
int negativeCounter;

int hit_counter;

//Boundingbox manually drawn
bool ended_drawing, drawing, caught_patch;
BoundingBox box;

void onMouseCB( int event, int x, int y, int flags, void* param);

class ImageConverter
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;

  image_transport::Subscriber depth_image_sub_;

  image_transport::Publisher image_pub_;
  ros::Publisher chatter_pub;
  ros::Publisher chatter_pub2;
  ros::Publisher detection_pub;

  tld::BoundingBox trackedBB;
  float realW, realH;
  Mat depth_image;

  volatile bool no;
  
public:
  ImageConverter()
    : it_(nh_)
  {
    // Subscrive to input video feed and publish output video feed
    /*image_sub_ = it_.subscribe("/image_raw", 1, 
      &ImageConverter::imageCb, this);*/
    image_sub_ = it_.subscribe("camera/rgb/image_color", 1, 
      &ImageConverter::imageCb, this);

    depth_image_sub_ = it_.subscribe("camera/depth/image_raw", 1, &ImageConverter::depthImageCb, this);

    image_pub_ = it_.advertise("/image_converter/output_video", 1);

    chatter_pub = nh_.advertise<std_msgs::String>("chatter", 1000);
    chatter_pub2 = nh_.advertise<std_msgs::String>("chatter2", 1000);
    detection_pub = nh_.advertise<std_msgs::Float64MultiArray>("detection_result", 100);

    namedWindow(OPENCV_WINDOW);



    sd.add_detector(&fd);
    sd.add_detector(&pfd);
    sd.add_detector(&upd);

    nnclassifier.load("positives.txt","negatives.txt");

    no = false;
    hit_counter = 0;

    std::ifstream in("pn_counters.txt");
    in >> positiveCounter >> negativeCounter;
    in.close();

    
    reset();
    

  }



  ~ImageConverter()
  {
    destroyWindow(OPENCV_WINDOW);


    nnclassifier.save("positives.txt","negatives.txt");
    
    std::ofstream out("pn_counters.txt");
    out << positiveCounter << " " << negativeCounter << std::endl;
    out.close();
  }

  void offilineTraining(char i_input = 'k')
  {
    nnclassifier.positives.clear();
    nnclassifier.negatives.clear();

    //True Positive Rate Before Using Depth Information
    double tpr0 = offilineTraining("positiveImages/pos_color_img","positiveImages/pos_depth_img",positiveCounter,'c',i_input);
    printf("Switching Negative Examples\n");
    //False Positive Rate Before Using Depth Information 
    double fpr0 = offilineTraining("negativeImages/neg_color_img","negativeImages/neg_depth_img",negativeCounter,'v',i_input);

    //True Positive Rate After Using Depth Information
    double tpr1 = offilineTrainingTest("positiveImages/pos_color_img","positiveImages/pos_depth_img",positiveCounter);

    //False Positive Rate After Using Depth Information
    double fpr1 = offilineTrainingTest("negativeImages/neg_color_img","negativeImages/neg_depth_img",negativeCounter);



    std::ofstream out("training_report.txt");

    out << "Size of Positive Set: " << positiveCounter << std::endl;
    out << "Size of Negative Set: " << negativeCounter << std::endl;
    out << "True Positive Rate Without Depth Info (TPR): " << tpr0 << std::endl;
    out << "False Positive Rate Without Depth Info (FPR): " << fpr0 << std::endl;
    out << "True Positive Rate With Depth Info (TPR): " << tpr1 << std::endl;
    out << "False Positive Rate With Depth Info (FPR): " << fpr1 << std::endl;
    out.close();

  }

  double offilineTrainingTest(char* color_prefix_path, char* depth_prefix_path, int set_size)
  {
    char filepath_color[256], filepath_depth[256];
    Mat orig_color_frame, orig_depth_frame;

    int n_valids = 0;
    for(int i = 0; i < set_size; i++)
    {
      printf("Testing example with NN %d for %s\n",i,depth_prefix_path);

      sprintf(filepath_color,"%s%d.bmp",color_prefix_path,i);
      sprintf(filepath_depth,"%s%d.txt",depth_prefix_path,i);
      orig_color_frame = imread(filepath_color,CV_LOAD_IMAGE_COLOR);
      orig_depth_frame = loadFromFile(filepath_depth);

      processCascadeOnly(orig_color_frame);
      box = trackedBB;
      Rect bb = box;
      if( trackedBB.valid ) {
        Mat roi = orig_depth_frame(box).clone();
        if( processNN(roi) )
          n_valids++;
      }

      
      

    }

    return double(n_valids)/set_size;
  }


  double offilineTraining(char* color_prefix_path, char* depth_prefix_path, int set_size, char c, char i_input = 'k')
  {
    char filepath_color[256], filepath_depth[256];
    Mat cpy_color_frame, orig_color_frame, orig_depth_frame;

    char input = i_input;
    int n_valids = 0;
    for(int i = 0; i < set_size; i++)
    {
      printf("Training example %d for %s\n",i,depth_prefix_path);
      reset();

      sprintf(filepath_color,"%s%d.bmp",color_prefix_path,i);
      sprintf(filepath_depth,"%s%d.txt",depth_prefix_path,i);
      orig_color_frame = imread(filepath_color,CV_LOAD_IMAGE_COLOR);
      orig_depth_frame = loadFromFile(filepath_depth);

      processCascadeOnly(orig_color_frame);
      box = trackedBB;
      Rect bb = box;
      if( trackedBB.valid ) {
        rectangle(orig_color_frame, bb.tl(), bb.br(), ORANGE, 2, CV_AA);
        n_valids++;
      }

      cpy_color_frame = orig_color_frame.clone();
      while( trackedBB.valid && input != 'n' && input != 'i' ) //While the user doesn't request the next image, enter the drawing loop
      {
        if( drawing ) {
          cpy_color_frame = orig_color_frame.clone();
          bb = box;
          rectangle(cpy_color_frame, bb.tl(), bb.br(), BLUE, 2, CV_AA);
        }

        if( input == 'r' )
        {
          reset();
          box = trackedBB;
        }

        imshow(TRAINING_WINDOW,cpy_color_frame);
        input = waitKey(20);
        
      }//if we leave the while loop, that means we have selected a boundingbox
      
      if(trackedBB.valid && input != 'i') {
        Mat roi = orig_depth_frame(box).clone();
        processNN(roi,c);
      }
      input = i_input;


      

    }

    return double(n_valids)/set_size;
  }

  void reset()
  {
      ended_drawing = drawing = caught_patch = false;
      box = BoundingBox();
  }



  void depthImageCb(const sensor_msgs::ImageConstPtr& msg)
  {
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::TYPE_16UC1);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

    
    processDepthImage(cv_ptr->image);

  }

  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

    if(depth_image.empty() ) return;

    processRGBImage(cv_ptr->image);

    // Output modified video stream
    image_pub_.publish(cv_ptr->toImageMsg());
  }

  void processCascadeOnly(Mat image)
  {
      trackedBB.valid = false;

      sd.detect(image);

      trackedBB = sd.detectedBB;

      realW = realDimensions[sd.detector_id].first;
      realH = realDimensions[sd.detector_id].second;

  }
  
  void processDepthImage(Mat image)
  {
      showDepthImage(image, "Depth Image");
      //imshow("DepthMat", image);

      depth_image = image;

      
  }

  void handleInput(Mat& image, char c)
  {
        //cin.get();

        if( c == 'c' )
        {
          
          sprintf(filepath,"positiveImages/pos_color_img%d.bmp",positiveCounter);
          imwrite(filepath,image);
          sprintf(filepath,"positiveImages/pos_depth_img%d.txt",positiveCounter++);
          saveToFile(depth_image,filepath);
          printf("Writing positive images!\n");

        }
        else if( c == 'v' )
        {

          sprintf(filepath,"negativeImages/neg_color_img%d.bmp",negativeCounter);
          imwrite(filepath,image);
          sprintf(filepath,"negativeImages/neg_depth_img%d.txt",negativeCounter++);
          saveToFile(depth_image,filepath);
          printf("Writing negative images!\n");
        }
  }


  bool processNN(Mat& roi, char c = 'k')
  {
        resize(roi,roi,Size(PATCH_DIM,PATCH_DIM),0,0,INTER_NEAREST);
        showDepthImage(roi, "Depth ROI Process NN");

        

        if( c == 'c' )
        {
          roi.convertTo(roi,DataType<double>::type,1);
          nnclassifier.train(roi,1);
        }
        else if( c == 'v' )
        {
          roi.convertTo(roi,DataType<double>::type,1);
          nnclassifier.train(roi,0);
        }
        
        


        return nnclassifier.filter(roi);
        
  }

  Mat toGrayScale(Mat& roi)
  {
    double min;
    double max;
    minMaxIdx(roi, &min, &max);
    Mat gray_roi;
    

    roi.convertTo(gray_roi,CV_8UC1, 255 / (max-min), -min);

    return gray_roi;
  }

  void showDepthImage(Mat& roi, char* window_name)
  {
    Mat gray_roi = toGrayScale(roi);

    Mat color_remapped_roi;
    applyColorMap(gray_roi,color_remapped_roi, COLORMAP_AUTUMN);

    imshow(window_name, color_remapped_roi);

  }

  bool processShape(Mat& roi, Mat& image)
  {
    showDepthImage(roi, "Depth ROI ProcessShape");
    

    /*
    Mat gray_roi = toGrayScale(roi);
    Mat grad;
    char* window_name = "Sobel Demo - Simple Edge Detector";
    int scale = 1;
    int delta = 0;
    int ddepth = CV_16S;

      /// Generate grad_x and grad_y
    Mat grad_x, grad_y;
    Mat abs_grad_x, abs_grad_y;

    /// Gradient X
    //Scharr( src_gray, grad_x, ddepth, 1, 0, scale, delta, BORDER_DEFAULT );
    Sobel( gray_roi, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT );
    convertScaleAbs( grad_x, abs_grad_x );

    /// Gradient Y
    //Scharr( src_gray, grad_y, ddepth, 0, 1, scale, delta, BORDER_DEFAULT );
    Sobel( gray_roi, grad_y, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT );
    convertScaleAbs( grad_y, abs_grad_y );

    /// Total Gradient (approximate)
    addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad );

    imshow(window_name, abs_grad_y );
    */
  }

  void processRGBImage(Mat image)
  {
      if( no ) return;

      no = true;

      processCascadeOnly(image);  


      float pos3D[] = {0.0f,0.0f,0.0f};

      Rect bb;
      Scalar color = ORANGE;

      char c = waitKey(10);
      handleInput(image,c);
      
      if( trackedBB.valid )
      {
        trackedBB.x = trackedBB.x - 15; if( trackedBB.x < 0 ) trackedBB.x = 0;
        trackedBB.y = trackedBB.y - 15; if( trackedBB.y < 0 ) trackedBB.y = 0;
        trackedBB.w = trackedBB.w + 15; if( trackedBB.w >= image.cols ) trackedBB.w = image.cols-1;
        trackedBB.h = trackedBB.h + 15; if( trackedBB.h >= image.rows ) trackedBB.h = image.rows-1;

        bb = trackedBB;
        std_msgs::String msg;
        uint16 median_distance = 0;

        
        Mat roi = depth_image(trackedBB).clone();


        bool isHuman = use_nn_classifier? processNN(roi,c) : processShape(roi,image);

        if( isHuman )
        {

          printf("YES, IT'S A HUMAN!\n");

          Mat sortedMat;

          cv::sort(roi.reshape(1,1), sortedMat, CV_SORT_EVERY_ROW + CV_SORT_ASCENDING);

          median_distance = sortedMat.at<uint16>((sortedMat.rows*sortedMat.cols)/2);

          color = BLUE;


          if( hit_counter >= 3 )
          {
            std_msgs::Float64MultiArray detection_result;

            detection_result.data.push_back(trackedBB.x);
            detection_result.data.push_back(trackedBB.y);
            detection_result.data.push_back(trackedBB.w);
            detection_result.data.push_back(trackedBB.h);
            detection_result.data.push_back(median_distance);

            detection_pub.publish(detection_result);
            hit_counter = 0;
          }
          else
          {
            hit_counter++;
          }

        }
        else
        {
          color = ORANGE;
        }
        
        std::stringstream ss;

        uint16 min = minElement(roi);
        

        ss << "\n#" << fd.detectionResult.size() << " human face(s) detected!" << std::endl;
        ss << "#" << pfd.detectionResult.size() << " human profile face(s) detected!" << std::endl;
        ss << "#" << upd.detectionResult.size() << " human upper body(ies) detected!" << std::endl;
        ss << "#" << "Median distance: " << median_distance << std::endl;
        ss << "#" << "Min distance: " << min << std::endl;

        estimate3DPos((bb.tl().x+bb.width/2), (bb.tl().y+bb.height/2), pos3D, 1.0f, bb.width, bb.height, realW, realH);
        
        ss << "Pos3D(" << pos3D[0] << ", " << pos3D[1] << ", " << pos3D[2] << ")" << std::endl;
        msg.data = ss.str();
        chatter_pub.publish(msg);

        rectangle(image, bb.tl(), bb.br(), color, 2, CV_AA);
        //TODO: Publish a tf with the estimated translation
      }

     
      // Update GUI Window
      imshow(OPENCV_WINDOW,image);
      waitKey(3);

      no = false;
  }
};





int main(int argc, char** argv)
{

  
  srand(time(NULL));
  ros::init(argc, argv, "human_detector");
  namedWindow(TRAINING_WINDOW);
  setMouseCallback(TRAINING_WINDOW, onMouseCB,NULL);
  ImageConverter ic;

  printf("number of args %d\n",argc);
  if(argc > 2)
  {
    if( argv[1][0] == '-' && argv[1][1] == 't' ) ic.offilineTraining(argv[2][0]);
    else 
    {
      printf("Usage: rosrun human_detection human_detection_node [<-t> <n|i|k>]\nOr: ./human_detection_node [<-t> <default_key_input>]\n");
      exit(1);
    }
  }
  else if( argc != 1 )
  {
    printf("Usage: rosrun human_detection human_detection_node [<-t> <n|i|k>]\nOr: ./human_detection_node [<-t> <default_key_input>]\n");
    exit(1);
  }

  ros::spin();
  return 0;
}


void onMouseCB( int event, int x, int y, int flags, void* param)
  {


      switch( event )
      {
      case CV_EVENT_MBUTTONDBLCLK:

          //First patch was selected
          //printf("First patch was selected!\n");

          //Some processing

          break;
      case CV_EVENT_MOUSEMOVE:

          if( drawing && !caught_patch )
          {
              box.xf = x;
              box.yf = y;
              box.w = abs(box.xf - box.x);
              box.h = abs(box.yf - box.y);

          }

          break;
      case CV_EVENT_LBUTTONDOWN:

          printf("mouse left button down!\n");

          if( ended_drawing && x >= box.x && x <= box.xf && y >= box.y && y <= box.yf && !caught_patch )
          {
              printf("Patch was selected!\n");
              caught_patch = true;

          }
          else if( !drawing && !caught_patch )
          {
              //Updating box's limits
              box.x = box.xf = x;
              box.y = box.yf = y;
              box.w = box.h = 0;
              ended_drawing = false;
              drawing = true;
          }

          break;
      case CV_EVENT_LBUTTONUP:

          ended_drawing = true;
          drawing = false;


          break;
      default:
          printf("another event\n");
          break;
      }


  }

